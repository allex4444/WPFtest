﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTest3.Models;

namespace WpfTest3.DBContext
{
    internal class MDBContext : DbContext
    {
        public DbSet<Faculty> Faculties { get; set; }
        public DbSet<Speciality> Specialities { get; set; }
        public DbSet<Student> Students { get; set; }

        public readonly string _path = @"C:\Users\GPMJo\source\repos\WpfTest3\WpfTest3\DB\AFCdb1.0.db";

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Data Source={_path}");
        }
    }
}
