﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WpfTest3.Migrations
{
    /// <inheritdoc />
    public partial class add_code_in_speciality : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Specialities",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Code",
                table: "Specialities");
        }
    }
}
