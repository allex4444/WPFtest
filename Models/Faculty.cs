﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest3.Models
{
    internal class Faculty
    {
        public int FacultyId {  get; set; }
        public string Name { get; set; }
        public virtual ObservableCollection<Speciality> Specialities { get; set; }
    }
}
