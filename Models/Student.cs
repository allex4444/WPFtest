﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest3.Models
{
    internal class Student
    {
        public int StudentId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Surname { get; set; }
        public int FacultyId { get; set; }
        public int SpecialityId { get; set; }

        public virtual Faculty Faculty { get; set; }
        public virtual Speciality Speciality { get; set; }
    }
}
