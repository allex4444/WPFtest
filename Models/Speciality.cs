﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfTest3.Models
{
    internal class Speciality
    {
        public int SpecialityId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public int FacultyId { get; set; }
        public virtual Faculty Faculty { get; set; }
    }
}
