﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfTest3.ViewModels;

namespace WpfTest3.Views
{
    /// <summary>
    /// Логика взаимодействия для FacultySpecialityControl.xaml
    /// </summary>
    public partial class FacultySpecialityControl : UserControl
    {
        public FacultySpecialityControl()
        {
            FacultySpecialityVM facultySpecialityVM = new FacultySpecialityVM();
            DataContext = facultySpecialityVM;

            InitializeComponent();
        }
    }
}
