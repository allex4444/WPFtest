﻿using MaterialDesignThemes.Wpf;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using WpfTest3.DBContext;
using WpfTest3.Models;

namespace WpfTest3.Data
{
    internal class DataWorker
    {
        public static ObservableCollection<Speciality>? GetSpecialites(int FacultyId)
        {
            using (MDBContext db = new MDBContext())
            {
                ObservableCollection<Speciality> result = new ObservableCollection<Speciality>(
                    db.Specialities.Where(s => s.FacultyId == FacultyId)
                );

                return result;
            }
        }

        public static ObservableCollection<Faculty>? GetFaculties()
        {
            using (MDBContext db = new MDBContext())
            {
                ObservableCollection<Faculty> result = new ObservableCollection<Faculty>(
                    db.Faculties.Include("Specialities").ToList()
                );

                return result;
            }
        }

        public static void CreateFaculty(Faculty faculty)
        {
            using (MDBContext db = new MDBContext())
            {
                db.Faculties.Add(faculty);
                db.SaveChanges();
            }
        }

        public static void RemoveFaculty(Faculty faculty)
        {
            using (MDBContext db = new MDBContext())
            {
                db.Faculties.Remove(faculty);

                db.SaveChanges();
            }
        }

        public static void CreateSpeciality(Speciality speciality) {

            using (MDBContext db = new MDBContext())
            {
                db.Specialities.Add(speciality);

                db.SaveChanges();
            }
        }

        public static void UpdateSpeciality(Speciality speciality)
        {
            using (MDBContext db = new MDBContext())
            {
                db.Specialities.Update(speciality);

                db.SaveChanges();
            }
        }

        public static void RemoveSpeciality(Speciality speciality)
        {
            using (MDBContext db = new MDBContext())
            {
                db.Specialities.Remove(speciality);
                
                db.SaveChanges();
            }
        }

        public static ObservableCollection<Student>? GetStudentList()
        {
            using (MDBContext db = new MDBContext())
            {
                ObservableCollection<Student> result = new ObservableCollection<Student>(
                    db.Students.Include(student => student.Faculty.Specialities).Include("Speciality").ToList()
                );

                return result;
            }
        }

        public static void CreateStudent(Student student)
        {
            using (MDBContext db = new MDBContext())
            {
                db.Students.Add(student);
                db.SaveChanges();
            }
        }
    }
}