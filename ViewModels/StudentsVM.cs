﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTest3.DBContext;
using WpfTest3.Models;
using WpfTest3.Data;
using System.Windows;
using WpfTest3.Views;

namespace WpfTest3.ViewModels
{
    partial class StudentsVM : ObservableObject
    {
        [ObservableProperty]
        public ObservableCollection<Student> students;

        [ObservableProperty]
        public ObservableCollection<Faculty> faculties;

        [ObservableProperty]
        public ObservableCollection<Speciality> specialities;

        private Student selectedStudent;

        private Faculty selectedFaculty;

        private Speciality selectedSpeciality;

        public Student SelectedStudent
        {
            get => selectedStudent;
            set
            {
                SetProperty(ref selectedStudent, value);
            }
        }

        public Faculty SelectedFaculty
        {
            get => selectedFaculty;
            set
            {
                if (SelectedStudent is not null)
                {
                    var changeStudent = new Student
                    {
                        FirstName = SelectedStudent.FirstName,
                        LastName = SelectedStudent.LastName,
                        Surname = SelectedStudent.Surname,
                        StudentId = SelectedStudent.StudentId,
                        Speciality = SelectedStudent.Speciality,
                        SpecialityId = SelectedStudent.SpecialityId,
                        FacultyId = value.FacultyId,
                        Faculty = value,
                    };

                    var index = Students.ToList().FindIndex(item => item.StudentId == SelectedStudent.StudentId);

                    Students[index] = changeStudent;
                }
            }
        }

        public Speciality SelectedSpeciality
        {
            get => selectedSpeciality;
            set
            {
                SetProperty(ref selectedSpeciality, value);

                if (SelectedStudent is not null)
                {
                    var index = Students.ToList().FindIndex(item => item.StudentId == SelectedStudent.StudentId);

                    Students[index].Speciality = value;
                    Students[index].SpecialityId = value.SpecialityId;
                }
                }
        }


        [RelayCommand]
        public void LoadStudents()
        {
            Students = DataWorker.GetStudentList();
        }

        public void LoadFaculties()
        {
            Faculties = DataWorker.GetFaculties();
        }


        [RelayCommand]
        public void AddStudent()
        {

        }

        [RelayCommand]
        public void UpdateStudent()
        {

        }

        [RelayCommand]
        public void RemoveStudent()
        {

        }

        [RelayCommand]
        public void OpenWindowCreateStudent()
        {
            Window CreateStudentWindow = new CreateStudentWindow();

            CreateStudentWindow.DataContext = this;
            CreateStudentWindow.Show();
        }

        public StudentsVM()
        {
            LoadFaculties();
            LoadStudents();
        }
    }
}
