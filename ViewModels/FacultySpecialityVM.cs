﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfTest3.Data;
using WpfTest3.DBContext;
using WpfTest3.Models;

namespace WpfTest3.ViewModels
{
    internal partial class FacultySpecialityVM : ObservableObject
    {
        [ObservableProperty]
        public string newFacultyName;

        [ObservableProperty]
        public string newSpecialityName;

        [ObservableProperty]
        public string newSpecialityCode;

        [ObservableProperty]
        public ObservableCollection<Faculty> faculties;

        private Faculty selectedFaculty;

        private Speciality selectedSpeciality;

        public Faculty SelectedFaculty
        {
            get => selectedFaculty;
            set => SetProperty(ref selectedFaculty, value);
        }

        [RelayCommand]
        public void LoadFaculties()
        {
            Faculties = DataWorker.GetFaculties();
        }

        [RelayCommand]
        public void CreateFaculty()
        {
            if (string.IsNullOrWhiteSpace(newFacultyName))
            {
                MessageBox.Show("Пустое поле", "Error", MessageBoxButton.OK);

                return;
            }

            var faculty = new Faculty { Name = newFacultyName };

            DataWorker.CreateFaculty(faculty);

            LoadFaculties();

            SelectedFaculty = Faculties.Last();
        }

        [RelayCommand]
        public void RemoveFaculty()
        {
            DataWorker.RemoveFaculty(SelectedFaculty);

            LoadFaculties();
        }

        public Speciality SelectedSpeciality
        {
            get => selectedSpeciality;
            set {
                SetProperty(ref selectedSpeciality, value);
            }
        }

        [RelayCommand]
        public void UpdateSpeciality()
        {
            DataWorker.UpdateSpeciality(SelectedSpeciality);

            var faculty = SelectedFaculty;

            LoadFaculties();

            SelectedFaculty = faculty;
        }

        [RelayCommand]
        public void RemoveSpeciality()
        {
            DataWorker.RemoveSpeciality(SelectedSpeciality);

            var faculty = SelectedFaculty;

            LoadFaculties();

            SelectedFaculty = faculty;
        }

        [RelayCommand]
        public void CreateSpeciality()
        {
            var speciality = new Speciality
            {
                Name = newSpecialityName,
                Code = newSpecialityCode,
                FacultyId = SelectedFaculty.FacultyId
            };

            DataWorker.CreateSpeciality(speciality);

            LoadFaculties();
        }

        [RelayCommand]
        public void OpenWindowCreateFaculty()
        {
            Window CreateFacultyWindow = new CreateFacultyWindow();

            CreateFacultyWindow.DataContext = this;
            CreateFacultyWindow.Show();
        }

        [RelayCommand]
        public void OpenWindowCreateSpeciality()
        {
            Window CreateSpecialityWindow = new CreateSpecialityWindow();

            CreateSpecialityWindow.DataContext = this;
            CreateSpecialityWindow.Show();
        }

        public FacultySpecialityVM()
        {
            LoadFaculties();
        }
    }
}
