﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfTest3.Views;

namespace WpfTest3.ViewModels
{
    partial class DashboardVM : ObservableObject
    {

        [ObservableProperty]
        public object currentView;

        [RelayCommand]
        public void OpenFacultySpeciality()
        {
            CurrentView = new FacultySpecialityControl();
        }

        [RelayCommand]
        public void OpenStudents()
        {
            CurrentView = new StudentsControl();
        }

        [RelayCommand]
        public void OpenStatement()
        {
            //CurrentView = new StatementWindow();
        }

        [RelayCommand]
        public void Logout()
        {
            Environment.Exit(0);
        }
    }
}
